#!/usr/bin/env fish

# Volume control
python3 keybindings/keybindings.py '<Alt>s' 'pavucontrol'
