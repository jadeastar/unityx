function settings_daemon --description 'Settings daemon'
    # Set GTK theme and background
    export GTK_THEME=(gsettings get org.gnome.desktop.interface gtk-theme | tail -c +2 | head -c -2)
    export GTK3_MODULES=(whereis -b libplotinus | awk '{print $2}')
    fish -c "while true; sleep 1 && feh --bg-fill (gsettings get org.gnome.desktop.background picture-uri | tail -c +9 | head -c -2); end" &
    xfwm4 &

    # Mkdir Desktop in the home directory
    mkdir ~/Desktop

    # Assign keybindings to rofi.
    fish -c "cd ~/Desktop && python3 "(echo $PWD)"/keybindings/keybindings.py '<Alt>w' 'rofi -theme "(echo $PWD)"/design/sidebar.rasi -show window' ''" &
    fish -c "cd ~/Desktop && python3 "(echo $PWD)"/keybindings/keybindings.py '<Super>a' 'rofi -theme "(echo $PWD)"/design/sidebar.rasi -show drun' ''" &
    fish -c "cd ~/Desktop && python3 "(echo $PWD)"/keybindings/keybindings.py '<Alt>a' 'rofi -theme "(echo $PWD)"/design/sidebar.rasi -show drun' ''" &

    # Add remaining keybindings.
    fish keybindings/add.fish &

    # Launch panel.
    sleep 1 && fish -c 'polybar -c design/panel.cfg default' &

    # Show widgets
    sleep 2 && fish -c 'for widget in ./design/widgets/*.conf; conky -c $widget &; end' &

    # Run polkit authentication agent.
    fish -c '/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 || /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' &

    # Execute autorun script, if it exists.
    fish -c '~/.config/unityx/autorun || true' &

    # Show the keyboard shortcuts dialog
    zenity --list \
      --class='UnityX' \
      --title='Keyboard Shortcuts' \
      --text='A list of keyboard shortcuts can be found below.' \
      --column='Shortcut'   --column='Definition' \
        'Alt+W'             'Show open windows' \
        'Alt+A or Super+A'  'Show applications' \
        'Alt+X'             'Terminate session' \
        'Alt+S'             'Volume control.' &

    # Show all the tray icons
    sleep 1
    nm-applet &
    blueman-applet &
    pnmixer &
    set lib_dir '/usr/lib/'(uname -i)'-linux-gnu'    
    $lib_dir/notify-osd &
    plotinus &

    # Assign keybinding to log out.
    python3 keybindings/keybindings.py '<Alt>x' 'exit' 'exit'

    # Kill all background processes launched
    kill (jobs -p)
end
